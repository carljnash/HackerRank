import Foundation


// MARK: - Arrays: Left Rotation
// https://www.hackerrank.com/challenges/ctci-array-left-rotation/problem

func rotateLeft(a: [Int], d: Int) -> [Int] {
    var rotatedArray = a
    for _ in 0..<d {
        rotatedArray.append(rotatedArray.removeFirst())
    }
    return rotatedArray
}

let array = [0,1,2,3,4,5]
let rotatedArray = rotateLeft(a: array, d: 3)
assert(rotatedArray == [3,4,5,0,1,2])
