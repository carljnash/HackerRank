import Foundation

// MARK: - Ransom Note
// https://www.hackerrank.com/challenges/ctci-ransom-note/problem

/// <#Description#>
///
/// - Parameters:
///   - magazine: <#magazine description#>
///   - note: <#note description#>
func checkMagazineWrong(magazine: [String], note: [String]) -> Void {
    
    // First make the arrays into strings so we can just work with them as characters
    var magazineString = magazine.joined()
    let noteString = note.joined()
    
    for noteChar in noteString {
        if magazineString.contains(noteChar) {
            let index = 0
            for magazineChar in magazineString {
                if magazineChar == noteChar {
                    magazineString
                    magazineString.remove(at: magazineString.index(magazineString.startIndex, offsetBy: index))
                    magazineString
                    break
                }
            }
        } else {
            print("Magazine string \(magazineString) doesn't contain: \(noteChar)")
            print("No")
            return
        }
    }
    
    print("Yes")
}


func checkMagazine(magazine: [String], note: [String]) -> Void {
    
    // make a mutable copy of the magazine so that we can remove matching words as we find them so we don't think we can use them again
    var magazineMutable = magazine
    
    // save the matching words we find so that we can make sure we've found all the words in the note at the end
    var matchingWords = [String]()
    
    // loop through each word in the note array and see if it exists in the magazine array
    for noteWord in note {
        guard magazine.contains(noteWord) else {
            print("No")
            return
        }
        
        // loop through each word in the magazine (mutable) array to see if we have one that matches the word from the note
        var magazineIndex = 0
        for magazineWord in magazineMutable {
            if magazineWord == noteWord {
                matchingWords.append(noteWord)
                magazineMutable.remove(at: magazineIndex)
                break
            }
            magazineIndex += 1
        }
        
    }
    
    if matchingWords == note {
        print("Yes")
    } else {
        print("No")
    }
}

let magazine = "apgo clm w lxkvg mwz elo bg elo lxkvg elo apgo apgo w elo bg".components(separatedBy: " ")
let note = "elo lxkvg bg mwz clm w".components(separatedBy: " ")

checkMagazine(magazine: magazine, note: note)
