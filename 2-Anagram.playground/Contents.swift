import Foundation

// MARK: - Make Anagram
// https://www.hackerrank.com/challenges/ctci-making-anagrams/problem


/// Compare two strings and calculate the total number of characters that must be deleted to be left with two strings that are an anagram of each other
///
/// - Parameters:
///   - a: The first string to compare
///   - b: The second string to compare
/// - Returns: An integer value of the amount of characters that must be removed to make both strings anagrams of each other
func makeAnagram(a: String, b: String) -> Int {
    
    var aMutable = a
    var bMutable = b
    
    var aAnagram = ""
    var bAnagram = ""
    
    for charA in a {
        if bMutable.contains(charA) {
            var index = 0
            for charB in bMutable {
                if charB == charA {
                    bAnagram.append(bMutable.remove(at: bMutable.index(bMutable.startIndex, offsetBy: index)))
                    break
                }
                index += 1
            }
        }
    }
    
    for charB in b {
        if aMutable.contains(charB) {
            var index = 0
            for charA in aMutable {
                if charA == charB {
                    aAnagram.append(aMutable.remove(at: aMutable.index(aMutable.startIndex, offsetBy: index)))
                    break
                }
                index += 1
            }
        }
    }
    
    let removedInA = a.count - aAnagram.count
    let removedInB = b.count - bAnagram.count
    let total = removedInA + removedInB
    
    return total
}

let a = "fcrxzwscanmligyxyvym"
let b = "jxwtrhvujlmrpdoqbisbwhmgpmeoke"

let numberOfCharactersRemoved = makeAnagram(a: a, b: b)
